﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers {
    public class HomeController : Controller {
        public IActionResult Index() {
            ViewBag.Message = DateTime.Now.ToString();
            #region task25
            // Hard coded in some supervisors to the SupervisorGroup static class
            /* Supervisor supervisor1 = new Supervisor {
                 Id = 1,
                 Name = "Per Hansen",
                 IsAvailable = true
             };
             Supervisor supervisor2 = new Supervisor {
                 Id = 2,
                 Name = "Jan Jansen",
                 IsAvailable = true
             };
             Supervisor supervisor3 = new Supervisor {
                 Id = 3,
                 Name = "Ole Jensen",
                 IsAvailable = true
             };

             SupervisorGroup.AddSupervisor(supervisor1);
             SupervisorGroup.AddSupervisor(supervisor2);
             SupervisorGroup.AddSupervisor(supervisor3);*/
            #endregion

            #region task26
            List<string> supervisorInfo = new List<string>();

            foreach(Supervisor s in SupervisorGroup.GetSampleSupervisorData()) {
                int? id = s?.Id ?? 6;
                string name = s?.Name ?? "No name";
                bool? available = s?.IsAvailable ?? false;

                supervisorInfo.Add($"ID: {id}, Name: {name}, Available: {available}");
            }

            return View("MyFirstView", supervisorInfo);
            #endregion
        }

        public IActionResult SupervisorListWithS() {
            // Where clause defaults to false if name.startswith is null
            return View(SupervisorGroup.GetSampleSupervisorData().Where(s => s?.Name.StartsWith("S") ?? false).Select(s => s));
        }

        [HttpGet]
        public IActionResult SupervisorInfo() {
            return View();
        }

        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor) {
            // Checks if input are valid corresponding to validation added in supervisor class
            if(ModelState.IsValid) {
                SupervisorGroup.AddSupervisor(supervisor);
                return View("AddSupervisorConfirmation", supervisor);
            } else {
                return View();
            }
        }

        [HttpGet]
        public IActionResult AllSupervisors() {
            return View(SupervisorGroup.Supervisors);
        }
    }
}
