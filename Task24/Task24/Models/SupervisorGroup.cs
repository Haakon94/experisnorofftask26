﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models {
    public static class SupervisorGroup {
        private static List<Supervisor> CurrentSupervisors = new List<Supervisor>();

        public static List<Supervisor> Supervisors {
            get { return CurrentSupervisors; }
        }

        public static void AddSupervisor(Supervisor newSupervisor) {
            CurrentSupervisors.Add(newSupervisor);
        }

        public static List<Supervisor> GetSampleSupervisorData() {
            Supervisor supervisor1 = new Supervisor { Id = 1, IsAvailable = true, Name = "Odd Hansen" };
            Supervisor supervisor2 = new Supervisor { Id = 2, IsAvailable = false, Name = "Martin Hansemann" };
            Supervisor supervisor3 = new Supervisor { Id = 3, IsAvailable = true, Name = "Sarah Hansemann" };
            Supervisor supervisor4 = new Supervisor { Id = 4, IsAvailable = false, Name = "Siv Sivertsen" };
            Supervisor supervisor5 = new Supervisor();

            List<Supervisor> supervisors = new List<Supervisor>();

            supervisors.Add(supervisor1);
            supervisors.Add(supervisor2);
            supervisors.Add(supervisor3);
            supervisors.Add(supervisor4);
            supervisors.Add(supervisor5);
            supervisors.Add(null);


            return supervisors;

        }
    }
}
